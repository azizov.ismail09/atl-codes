//package com.springboot.tutorial.repository;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import com.springboot.tutorial.entity.Student;
//import java.util.Optional;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.jdbc.Sql;
//import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@RunWith(SpringRunner.class)
//@ActiveProfiles(profiles = "test")
//@EnableConfigurationProperties
//@EnableJpaRepositories
//class StudentRepositoryTest {
//
//  @Autowired
//  private StudentRepository studentRepository;
//
//  @Test
//  @Sql(scripts = "classpath:sql/students.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//  public void givenFindStudentByIdWhenFoundThenReturnEntity() {
//    // arrange
//    Long id = 10L;
//
//    // act
//    Optional<Student> result = studentRepository.findById(id);
//
//    // assert
//    assertFalse(result.isEmpty());
//
//    Student student = result.get();
//    assertEquals(id, student.getId());
//    assertEquals(25, student.getAge());
//    assertEquals("Test", student.getName());
//    assertEquals("Testov", student.getSurname());
//  }
//
//}