//package com.springboot.tutorial.service;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import com.springboot.tutorial.entity.User;
//import com.springboot.tutorial.repository.UserRepository;
//import java.util.List;
//import java.util.Optional;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentMatchers;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.assertj.core.api.Assertions;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//
//@ExtendWith(MockitoExtension.class)
//class CustomUserDetailsServiceTest {
//
//  @InjectMocks
//  private CustomUserDetailsService userDetailsService;
//
//  @Mock
//  private UserRepository userRepository;
//
//  private User user;
//
//  @BeforeEach
//  void setUp() {
//    user = new User();
//    user.setUsername("test");
//    user.setRole("ADMIN");
//    user.setPassword("123456");
//    user.setId(1L);
//  }
//
//  @AfterEach
//  void rollback() {
//    // some logic
//  }
//
//  @Test
//  public void givenLoadUserByUsernameWhenFoundThenReturnResult() {
//    // arrange
//    String username = "test";
//    Mockito.when(userRepository.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.of(user));
//
//    // act
//    UserDetails result = userDetailsService.loadUserByUsername(username);
//
//    // assert
//    assertNotNull(result);
//    assertEquals(user.getUsername(), result.getUsername());
//    assertEquals(user.getPassword(), result.getPassword());
//  }
//
//  @Test
//  public void givenLoadUserByUsernameWhenNotFoundThenThrowException() {
//    // arrange
//    String username = "test";
//    Mockito.when(userRepository.findByUsername(ArgumentMatchers.anyString())).thenReturn(Optional.empty());
//
//    // act & assert
//    Assertions.assertThatThrownBy(
//              () -> userDetailsService.loadUserByUsername(username))
//            .isInstanceOf(UsernameNotFoundException.class)
//            .hasMessage("Invalid username");
//  }
//
//}