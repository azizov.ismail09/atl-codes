package com.springboot.tutorial.util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
class CalculatorUtilTest {

  @InjectMocks
  private CalculatorUtil calculatorUtil;

  @Test
  public void testSum() {
    int a = 3;
    int b = 10;

    int result = calculatorUtil.calc(a, b);

    int expectedResult = b - a;

    assertEquals(expectedResult, result);

    a = 10;
    b = 3;
    result = calculatorUtil.calc(a, b);
    expectedResult = a - b;
    assertEquals(expectedResult, result);
  }

}