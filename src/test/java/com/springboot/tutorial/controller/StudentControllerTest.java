//package com.springboot.tutorial.controller;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import com.springboot.tutorial.TutorialApplication;
//import com.springboot.tutorial.dto.ErrorMessage;
//import com.springboot.tutorial.dto.StudentDto;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.test.web.server.LocalServerPort;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.jdbc.Sql;
//import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
//
//@SpringBootTest(classes = TutorialApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
//@ActiveProfiles(profiles = "test")
//class StudentControllerTest {
//
//  @LocalServerPort
//  private int port;
//
//  private String url;
//
//  @Autowired
//  private TestRestTemplate restTemplate;
//
//  @BeforeEach
//  void setUp() {
//    this.url = "http://localhost:" + port + "/testapp/students";
//  }
//
//  @Test
//  @Sql(scripts = "classpath:sql/students.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//  public void givenGetStudentByIdWhenFoundThenReturnResultWith200() {
//    // arrange
//    Long id = 1L;
//
//    // act
//    ResponseEntity<StudentDto> response = restTemplate.getForEntity(url + "/" + id, StudentDto.class);
//
//    // assert
//    assertNotNull(response.getBody());
//    assertEquals(HttpStatus.OK, response.getStatusCode());
//
//    final StudentDto student = response.getBody();
//    assertEquals(id, Integer.toUnsignedLong(student.getId()));
//    assertEquals(25, student.getAge());
//    assertEquals("Test", student.getFirstName());
//    assertEquals("Testov", student.getSurname());
//  }
//
//  @Test
//  public void givenGetStudentByIdWhenNotFoundThenReturnResultWith404() {
//    // arrange
//    long id = 1L;
//
//    // act
//    ResponseEntity<ErrorMessage> response = restTemplate.getForEntity(url + "/" + id, ErrorMessage.class);
//
//    // assert
//    assertNotNull(response.getBody());
//    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//
//    final ErrorMessage errorMessage = response.getBody();
//    assertEquals("Student not found with such id", errorMessage.getErrorMessage());
//  }
//
//}