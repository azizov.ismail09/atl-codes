package com.springboot.tutorial.validator.impl;

import com.springboot.tutorial.validator.ValidPhoneNumber;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return value.startsWith("+994");
  }

}
