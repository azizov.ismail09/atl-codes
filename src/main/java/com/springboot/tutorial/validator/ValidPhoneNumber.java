package com.springboot.tutorial.validator;

import com.springboot.tutorial.validator.impl.PhoneNumberValidator;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = PhoneNumberValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidPhoneNumber {

  public String message() default "Invalid phone number prefix";

  public Class<?>[] groups() default {};

  public Class<? extends Payload>[] payload() default {};

}
