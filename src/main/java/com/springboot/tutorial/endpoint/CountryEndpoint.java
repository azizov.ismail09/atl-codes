package com.springboot.tutorial.endpoint;

import com.springboot.tutorial.gen.Country;
import com.springboot.tutorial.gen.Currency;
import com.springboot.tutorial.gen.GetCountryRequest;
import com.springboot.tutorial.gen.GetCountryResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CountryEndpoint {

  private static final String NAMESPACE_URI = "http://www.springboot.com/tutorial/gen";

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
  @ResponsePayload
  public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
    GetCountryResponse response = new GetCountryResponse();
    Country country = new Country();
    country.setName("Azerbaijan");
    country.setCurrency(Currency.EUR);
    country.setPopulation(10000000);
    country.setCapital("Baku");
    response.setCountry(country);
    return response;
  }

}
