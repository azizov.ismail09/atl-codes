package com.springboot.tutorial.client;

import com.springboot.tutorial.dto.BookDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient
public interface BookClient {

  @PostMapping(value = "/test/books", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<BookDto> createBook(BookDto bookDto);

//  @GetMapping(value = "/authors/{authorId}/books/{bookId}/details", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//  ResponseEntity<BookDto> getBookById(@PathVariable Long authorId, @PathVariable Long bookId);

}

