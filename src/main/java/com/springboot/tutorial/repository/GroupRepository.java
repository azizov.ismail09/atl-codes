package com.springboot.tutorial.repository;

import com.springboot.tutorial.entity.StudentGroup;
import java.util.List;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<StudentGroup, Long> {

//  @EntityGraph(type = EntityGraphType.FETCH, value = "student_groups_graph")
//  List<StudentGroup> findAll();

  @EntityGraph(type = EntityGraphType.FETCH, attributePaths = { "students", "students.studentInfo", "students.teachers" })
  List<StudentGroup> findAll();

}
