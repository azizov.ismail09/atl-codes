package com.springboot.tutorial.repository;

import com.springboot.tutorial.entity.Student;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

  List<Student> findAllByNameLike(String name);

  List<Student> findAllByNameLikeOrSurnameLikeOrderById(String name, String surname);

//  @Query("select s from Student s join s.teachers t where t.age > :age")
//  List<Student> searchStudentsWithTeacherAgeGreaterThan(int age);

  @Query(value = "select\n"
          + "            student0_.id as id,\n"
          + "            student0_.age as age,\n"
          + "            student0_.group_id as group_id,\n"
          + "            student0_.name as name,\n"
          + "            student0_.student_info_id as student_info_id,\n"
          + "            student0_.surname as surname\n"
          + "        from\n"
          + "            students student0_\n"
          + "        inner join\n"
          + "            teachers_students teachers1_\n"
          + "                on student0_.id=teachers1_.student_id\n"
          + "        inner join\n"
          + "            teachers teacher2_\n"
          + "                on teachers1_.teacher_id=teacher2_.id\n"
          + "        where\n"
          + "            teacher2_.age > ?", nativeQuery = true)
  List<Student> searchStudentsWithTeacherAgeGreaterThan(int age);

}


