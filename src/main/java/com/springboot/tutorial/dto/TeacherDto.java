package com.springboot.tutorial.dto;

import lombok.Data;

@Data
public class TeacherDto {

  private Long id;
  private String name;
  private String surname;
  private int age;

}
