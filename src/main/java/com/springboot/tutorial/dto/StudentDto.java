package com.springboot.tutorial.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class StudentDto {

  private Integer id;
  private String firstName;
  private String surname;
  private Integer age;
  private String address;
  private String phoneNumber;
  private Boolean adult;
  private String gender;

  private List<TeacherDto> teachers = new ArrayList<>();

}
