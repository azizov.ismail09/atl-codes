package com.springboot.tutorial.dto;

import java.time.LocalDateTime;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ErrorMessage {

  private HttpStatus status;
  private LocalDateTime now;
  private String errorMessage;
  private int errorCode;

}
