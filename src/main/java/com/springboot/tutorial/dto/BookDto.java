package com.springboot.tutorial.dto;

//import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString(exclude = {"id"})
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDto {

  @NotNull
//  @ApiModelProperty(notes = "Book ID", example = "1", required = true)
  private int id;

  @NotEmpty
//  @ApiModelProperty(notes = "Book name", example = "Head first java", required = true)
  private String name;

}
