package com.springboot.tutorial.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class StudentGroupDto {

  private Long id;
  private String name;
  private String number;

  private List<StudentDto> students = new ArrayList<>();
}
