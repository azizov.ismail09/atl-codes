package com.springboot.tutorial.config;

import com.springboot.tutorial.exception.InvalidArgumentException;
import com.springboot.tutorial.exception.NotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {

  @Override
  public Exception decode(String s, Response response) {
    switch (response.status()) {
      case 400:
        throw new InvalidArgumentException("Invalid parameters");
      case 404:
        throw new NotFoundException("No resource found");
    }

    return new RuntimeException("Unknown exception");
  }

}
