package com.springboot.tutorial.config;

import com.springboot.tutorial.service.CreditService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class ScheduledConfig {

  private final CreditService creditService;

  @Scheduled(cron = "*/3 * * * * *")
  public void sayHello() {
    creditService.checkCredit();
  }

}

