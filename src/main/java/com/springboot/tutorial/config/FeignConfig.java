package com.springboot.tutorial.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.tutorial.client.BookClient;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients
public class FeignConfig {

  @Autowired
  private ObjectMapper objectMapper;

  @Bean
  public BookClient bookClient() {
    return getClient(BookClient.class, "http://localhost:8099/testapp");
  }

  private <T> T getClient(final Class<T> clazz, final String baseUrl) {
    return Feign.builder()
            .contract(new SpringMvcContract())
            .client(new OkHttpClient())
            .decoder(new JacksonDecoder(objectMapper))
            .encoder(new JacksonEncoder(objectMapper))
            .errorDecoder(new CustomErrorDecoder())
            .target(clazz, baseUrl);
  }

}
