package com.springboot.tutorial.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "teachers")
@Data
@EqualsAndHashCode
public class Teacher {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private String surname;
  private int age;

  @ManyToMany(mappedBy = "teachers")
  private List<Student> students = new ArrayList<>();

  public void addStudent(Student student) {
    this.students.add(student);
    student.getTeachers().add(this);
  }

}
