package com.springboot.tutorial.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "students")
@Getter
@Setter
@EqualsAndHashCode(exclude = {"studentInfo", "group", "teachers"})
public class Student {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false, length = 50)
  private String name;

  @Column(name = "surname", nullable = false, length = 50)
  private String surname;

  private Integer age;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "student_info_id")
  private StudentInfo studentInfo;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "group_id")
  private StudentGroup group;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(
          name = "teachers_students",
          joinColumns = { @JoinColumn(name = "student_id") },
          inverseJoinColumns = { @JoinColumn(name = "teacher_id") }
  )
  private Set<Teacher> teachers = new HashSet<>();

  public void setGroup(StudentGroup group) {
    this.group = group;
    group.getStudents().add(this);
  }

}
