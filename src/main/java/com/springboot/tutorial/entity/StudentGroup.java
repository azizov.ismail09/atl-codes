package com.springboot.tutorial.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "student_groups")
@Setter
@Getter
@NamedEntityGraph(name = "student_groups_graph",
        attributeNodes = @NamedAttributeNode(value = "students", subgraph = "subgraph.student"),
        subgraphs = {
          @NamedSubgraph(name = "subgraph.student", attributeNodes = @NamedAttributeNode(value = "studentInfo")),
          @NamedSubgraph(name = "subgraph.student", attributeNodes = @NamedAttributeNode(value = "teachers"))
        })
public class StudentGroup {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;
  private String number;

  @OneToMany(mappedBy = "group")
  private Set<Student> students = new HashSet<>();

}
