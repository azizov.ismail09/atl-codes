package com.springboot.tutorial.model;

public enum TokenType {
	ACCESS_TOKEN, REFRESH_TOKEN
}
