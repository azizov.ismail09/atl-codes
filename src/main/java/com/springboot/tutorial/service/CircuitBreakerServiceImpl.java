package com.springboot.tutorial.service;

import com.springboot.tutorial.dto.StudentDto;
import com.springboot.tutorial.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class CircuitBreakerServiceImpl implements CircuitBreakerService {

  private final CircuitBreakerFactory circuitBreakerFactory;

  @Override
  public StudentDto getStudentById(Long id) {
    RestTemplate restTemplate = new RestTemplate();
    CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");

    String url = "http://localhost:8099/testapp/students/" + id;

    return circuitBreaker.run(
            () -> restTemplate.getForObject(url, StudentDto.class),
            throwable -> getBackupStudentInfo(throwable)
    );
  }

  private StudentDto getBackupStudentInfo(Throwable throwable) {
    throw new NotFoundException(throwable.getMessage());
  }

}
