package com.springboot.tutorial.service;

import com.springboot.tutorial.controller.request.RegisterDto;

public interface AuthService {


  void register(RegisterDto registerDto);

  String hash(String msg);

}
