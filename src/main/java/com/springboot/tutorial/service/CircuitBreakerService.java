package com.springboot.tutorial.service;

import com.springboot.tutorial.dto.StudentDto;

public interface CircuitBreakerService {

  StudentDto getStudentById(Long id);

}
