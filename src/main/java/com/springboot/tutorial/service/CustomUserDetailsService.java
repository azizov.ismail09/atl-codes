package com.springboot.tutorial.service;

import com.springboot.tutorial.entity.User;
import com.springboot.tutorial.repository.UserRepository;
import com.springboot.tutorial.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("Invalid username"));
    UserPrincipal userPrincipal = new UserPrincipal();
    userPrincipal.setId(user.getId());
    userPrincipal.setUsername(user.getUsername());
    userPrincipal.setPassword(user.getPassword());
    userPrincipal.setRole(user.getRole());
    return userPrincipal;
  }

}
