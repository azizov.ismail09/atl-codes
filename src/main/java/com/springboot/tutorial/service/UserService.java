package com.springboot.tutorial.service;

import com.springboot.tutorial.entity.Student;
import java.util.List;

public interface UserService {

  String getUserById(Long id);

}
