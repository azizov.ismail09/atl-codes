package com.springboot.tutorial.service;

import com.springboot.tutorial.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

  @Override
  public String getUserById(Long id) {
    if (id == 1) {
      return "Testov Test";
    } else {
      throw new NotFoundException("User not found with such id");
    }
  }

}
