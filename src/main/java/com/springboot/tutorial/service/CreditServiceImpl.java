package com.springboot.tutorial.service;

import org.springframework.stereotype.Service;

@Service
public class CreditServiceImpl implements CreditService {

  @Override
  public void checkCredit() {
    System.out.println("Started credit payment checking...");
  }

}
