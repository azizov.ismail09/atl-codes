package com.springboot.tutorial.controller.request;

import lombok.Data;

@Data
public class RegisterDto {

  private String name;
  private String username;
  private String password;

}
