package com.springboot.tutorial.controller.request;

import com.springboot.tutorial.validator.GroupValidator;
import com.springboot.tutorial.validator.StudentValidator;
import com.springboot.tutorial.validator.TeacherValidator;
import com.springboot.tutorial.validator.ValidPhoneNumber;
import javax.persistence.Column;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class StudentRequest {


  private String teacherName;
  private String teacherSurname;
  @Max(value = 65, groups = TeacherValidator.class)
  @Positive(groups = TeacherValidator.class)
  private int teacherAge;

  @NotNull(message = "Group name cannot be null", groups = GroupValidator.class)
  private String groupName;
  private String groupNumber;

  @NotEmpty(groups = StudentValidator.class)
  private String name;

  @NotBlank(groups = StudentValidator.class)
  private String surname;

//  @Min(value = 17)
  @Max(value = 65, groups = StudentValidator.class)
  @Positive(groups = StudentValidator.class)
  private Integer age;
  private String address;

  @Size(min = 10, max = 12, groups = StudentValidator.class)
  @ValidPhoneNumber(groups = StudentValidator.class)
  private String phoneNumber;

  @Email(groups = StudentValidator.class)
  private String email;

  @Pattern(regexp = "^[0-9]{5}(?:-[0-9]{4})?$", message = "Invalid postal code", groups = StudentValidator.class)
  private String postalCode;

  @AssertTrue(groups = StudentValidator.class)
  private Boolean accepted;

}
