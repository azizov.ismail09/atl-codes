package com.springboot.tutorial.controller;

import com.springboot.tutorial.client.BookClient;
import com.springboot.tutorial.dto.BookDto;
import com.springboot.tutorial.dto.StudentDto;
import com.springboot.tutorial.exception.InsufficientBalanceException;
import com.springboot.tutorial.exception.NotFoundException;
import com.springboot.tutorial.util.FileUtil;
import com.springboot.tutorial.checker.BalanceChecker;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
@Slf4j
public class TestController {

//  private final Logger log = LoggerFactory.getLogger(TestController.class);

  @Value("${files.extensions:xlsx,docx}")
  private String[] fileExtension;

  private final BalanceChecker balanceChecker;

  private final Path path = Paths.get("/Users/ismayil/Downloads/tutorial");

  private final FileUtil fileUtil;

  private final BookClient bookClient;

//  private final AuthService authService;

  //  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  @ApiOperation(value = "Say hello", notes = "Says hello on calling method")
  @ApiResponses(value = {
          @ApiResponse(code = 200, message = "Successfully greeting"),
          @ApiResponse(code = 400, message = "Required name field")
  })
  @GetMapping("/hello")
  public String sayHello(@ApiParam(name = "name", value = "Person name", example = "Test") @RequestParam("name") String name,
          @ApiParam(name = "age", value = "Person age", example = "20", defaultValue = "20") @RequestParam(defaultValue = "20") int age) {
    log.debug("Debug level log message");
    log.warn("Warn level log message");
    log.error("Error level log message");
    log.trace("Trace level log message");
    log.info("Info level log message");
    log.trace("Trace level log message");
    return "Hello, " + name + ", age " + age;
  }

  @GetMapping("/hello/names")
  public void sayHelloNames(@RequestParam List<String> names) {
    names.forEach(n -> System.out.println("Hello, " + n));
  }

  @GetMapping("/authors/{authorId}/books/{bookId}/details")
  public ResponseEntity<String> getBookById(@PathVariable int authorId, @PathVariable int bookId) {
    throw new NotFoundException("Not found book with such author or book ids");
//    return ResponseEntity.ok("Book details by id " + bookId + " of author with id " + authorId);
  }

  @PostMapping("/books")
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<BookDto> createBook(@Valid @RequestBody BookDto bookDto)
          throws InterruptedException {
//    Thread.sleep(5000);
    System.out.println(bookDto);
    return ResponseEntity.ok().header("Some header").body(bookDto);
  }

  @PostMapping("/money-transfer")
  public void makeMoneyTransfer(@RequestParam double amount) {
    balanceChecker.check(amount);
  }

//  @PostMapping("/auth")
//  public ResponseEntity<?> login(@RequestParam String username, @RequestParam String password) {
//    return ResponseEntity.ok(authService.login(username, password));
//  }

  @PostMapping("/upload")
  public void uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
    log.info("Size: {}", file.getSize());
    log.info("File name: {}", file.getOriginalFilename());
    Files.copy(file.getInputStream(), this.path.resolve(file.getOriginalFilename()));
  }

  @GetMapping("/download")
  public ResponseEntity<Resource> download(@RequestParam String fileName) {
    Resource resource = fileUtil.load(fileName, this.path);
    return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
            .body(resource);
  }

  @ExceptionHandler(InsufficientBalanceException.class)
  public ResponseEntity<?> handleInsufficientBalanceException(InsufficientBalanceException ex) {
    log.info("Occured exception:", ex);
    return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
  }

  @GetMapping("/call-getting-student")
  public ResponseEntity<StudentDto> callStudentById(@RequestParam Long studentId) {
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity response = restTemplate.getForEntity("http://localhost:8099/testapp/students/" + studentId, StudentDto.class);
    if (response.getStatusCode() == HttpStatus.NOT_FOUND) {
      throw new NotFoundException("Invalid student id");
    }
    StudentDto studentDto = (StudentDto) response.getBody();
    return ResponseEntity.ok(studentDto);
  }

//  @PostMapping("call-create-book")
//  public ResponseEntity<BookDto> callCreateBook(@RequestBody BookDto bookDto) {
//    RestTemplate restTemplate = new RestTemplate();
////    restTemplate.postForObject("http://localhost:8099/testapp/test/books", bookDto, BookDto.class);
//    HttpEntity httpEntity = new HttpEntity(bookDto);
//    return restTemplate.exchange("http://localhost:8099/testapp/test/books", HttpMethod.POST, httpEntity, BookDto.class);
//  }

  @PostMapping("call-create-book")
  public ResponseEntity<BookDto> callCreateBook(@RequestBody BookDto bookDto) {
    return bookClient.createBook(bookDto);
  }

  @GetMapping("/call-get-book")
  public ResponseEntity<BookDto> callGetBookById() {
    return ResponseEntity.ok(null);
  }

}
