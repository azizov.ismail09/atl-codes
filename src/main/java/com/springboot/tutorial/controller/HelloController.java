package com.springboot.tutorial.controller;

import com.springboot.tutorial.dto.StudentDto;
import com.springboot.tutorial.entity.Student;
import com.springboot.tutorial.repository.StudentRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloController {

  private List<StudentDto> students = new ArrayList<>();
  private Integer id = 1;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ModelAndView index(Locale locale) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("hello/index");
    modelAndView.addObject("students", students);
    modelAndView.addObject("studentForm", new StudentDto());
    modelAndView.addObject("imageName", "unnamed.png");
    return modelAndView;
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public ModelAndView addStudent(StudentDto studentDto) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("hello/index");

    studentDto.setId(id++);
    this.students.add(studentDto);

    modelAndView.addObject("students", students);
    modelAndView.addObject("studentForm", new StudentDto());
    return modelAndView;
  }

  @RequestMapping(value = "/details", method = RequestMethod.GET)
  public ModelAndView getDetails(@RequestParam Integer studentId) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("hello/view");
    StudentDto student = students.stream().filter(s -> s.getId().equals(studentId)).findFirst().get();
    modelAndView.addObject("student", student);
    return modelAndView;
  }

}

