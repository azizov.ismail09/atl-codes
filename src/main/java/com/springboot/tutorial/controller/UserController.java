package com.springboot.tutorial.controller;

import com.springboot.tutorial.service.UserService;
import javax.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;

  @RolesAllowed("ROLE_EDITOR")
  @GetMapping("/{id}")
  public String findUserById(@PathVariable Long id) {
    return userService.getUserById(id);
  }

}

