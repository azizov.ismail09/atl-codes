package com.springboot.tutorial.controller;

import com.springboot.tutorial.controller.request.LoginRequest;
import com.springboot.tutorial.controller.request.RegisterDto;
import com.springboot.tutorial.dto.TokenDTO;
import com.springboot.tutorial.model.TokenType;
import com.springboot.tutorial.security.JwtTokenProvider;
import com.springboot.tutorial.security.UserPrincipal;
import com.springboot.tutorial.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

  private final JwtTokenProvider tokenProvider;
  private final AuthenticationManager authenticationManager;
  private final AuthService authService;

  @PostMapping("/login")
  public ResponseEntity<TokenDTO> login(@RequestBody LoginRequest request) {
    Authentication authentication =
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getUsername(),
                            request.getPassword()
                    )
            );

    SecurityContextHolder.getContext().setAuthentication(authentication);
    TokenDTO tokenDTO = new TokenDTO();
    tokenDTO.setAccessToken(tokenProvider.generateToken((UserPrincipal) authentication.getPrincipal(), TokenType.ACCESS_TOKEN));
    tokenDTO.setRefreshToken(tokenProvider.generateToken((UserPrincipal) authentication.getPrincipal(), TokenType.REFRESH_TOKEN));
    return ResponseEntity.ok(tokenDTO);
  }

  @PostMapping("/refresh")
  public ResponseEntity<TokenDTO> refreshToken(@AuthenticationPrincipal UserPrincipal userPrincipal) {
    TokenDTO tokenDTO = new TokenDTO();
    tokenDTO.setAccessToken(tokenProvider.generateToken(userPrincipal, TokenType.ACCESS_TOKEN));
    tokenDTO.setRefreshToken(tokenProvider.generateToken(userPrincipal, TokenType.REFRESH_TOKEN));
    return ResponseEntity.ok(tokenDTO);
  }

  @PostMapping("/register")
  public void register(@RequestBody RegisterDto registerDto) {
    authService.register(registerDto);
  }

  @GetMapping("/hash")
  public ResponseEntity<String> getHashedMsg(@RequestParam String msg) {
    return ResponseEntity.ok(authService.hash(msg));
  }

}