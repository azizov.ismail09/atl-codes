package com.springboot.tutorial.controller;

import com.springboot.tutorial.controller.request.StudentRequest;
import com.springboot.tutorial.dto.StudentDto;
import com.springboot.tutorial.dto.StudentGroupDto;
import com.springboot.tutorial.dto.TeacherDto;
import com.springboot.tutorial.entity.Student;
import com.springboot.tutorial.entity.StudentGroup;
import com.springboot.tutorial.entity.StudentInfo;
import com.springboot.tutorial.entity.Teacher;
import com.springboot.tutorial.exception.NotFoundException;
import com.springboot.tutorial.mapper.StudentMapper;
import com.springboot.tutorial.repository.GroupRepository;
import com.springboot.tutorial.repository.StudentInfoRepository;
import com.springboot.tutorial.repository.StudentRepository;
import com.springboot.tutorial.validator.StudentValidator;
import com.springboot.tutorial.validator.TeacherValidator;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

  private final StudentRepository studentRepository;
  private final GroupRepository groupRepository;
  private final StudentInfoRepository studentInfoRepository;

  @PostMapping
  public void saveStudent(@Validated(StudentValidator.class) @RequestBody StudentRequest studentRequest) {
//    StudentGroup group = groupRepository.findById(studentRequest.getGroupId())
//            .orElseThrow(() -> new NotFoundException("Group not found with such id"));


    StudentGroup group = new StudentGroup();
    group.setName(studentRequest.getGroupName());
    group.setNumber(studentRequest.getGroupNumber());

    final Student student = new Student();
    student.setName(studentRequest.getName());
    student.setSurname(studentRequest.getSurname());
    student.setAge(studentRequest.getAge());
    student.setGroup(group);

    final StudentInfo studentInfo = new StudentInfo();
    studentInfo.setAddress(studentRequest.getAddress());
    studentInfo.setPhoneNumber(studentRequest.getPhoneNumber());

    student.setStudentInfo(studentInfo);

    Teacher teacher = new Teacher();
    teacher.setName(studentRequest.getTeacherName());
    teacher.setSurname(studentRequest.getTeacherSurname());
    teacher.setAge(studentRequest.getTeacherAge());
    teacher.addStudent(student);

    studentRepository.save(student);
  }

  @GetMapping("/{id}")
  public ResponseEntity<StudentDto> getStudentById(@PathVariable Long id) {
    return ResponseEntity.ok(StudentMapper.INSTANCE.entityToDto(studentRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Student not found with such id"))));
  }

  @PutMapping("/{id}")
  public void updateStudent(@RequestBody Student student, @PathVariable Long id) {
    Student oldStudent = studentRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Student not found with such id"));
    oldStudent.setName(student.getName());
    oldStudent.setSurname(student.getSurname());
    oldStudent.setAge(student.getAge());
//    oldStudent.setPhoneNumber(student.getPhoneNumber());
    studentRepository.save(oldStudent);
  }

  @PatchMapping("/{id}/phoneNumber/{phoneNumber}")
  public void updatePhoneNumber(@PathVariable Long id, @PathVariable String phoneNumber) {
    Student oldStudent = studentRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Student not found with such id"));
//    oldStudent.setPhoneNumber(phoneNumber);
    studentRepository.save(oldStudent);
  }

  @DeleteMapping("/{id}")
  public void deleteStudentInfo(@PathVariable Long id) {
    Student student = studentRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Student not found with such id"));
    studentRepository.delete(student);
  }

  @GetMapping("/search/{nameOrSurname}")
  public ResponseEntity<List<StudentDto>> search(@PathVariable String nameOrSurname, @RequestParam int age) {
    var students = studentRepository.searchStudentsWithTeacherAgeGreaterThan(age);
    List<StudentDto> studentDtoList = new ArrayList<>();
    for (var student : students) {
      StudentDto studentDto = new StudentDto();
      studentDto.setFirstName(student.getName());
      studentDto.setSurname(student.getSurname());
      studentDto.setAge(student.getAge());
      studentDto.setPhoneNumber(student.getStudentInfo().getPhoneNumber());
      studentDto.setAddress(student.getStudentInfo().getAddress());

      studentDtoList.add(studentDto);
    }

    return ResponseEntity.ok(studentDtoList);
  }

  @GetMapping("/groups")
  public ResponseEntity<List<StudentGroupDto>> getGroups() {
    final List<StudentGroup> groups = groupRepository.findAll();
    final List<StudentGroupDto> groupDtos = new ArrayList<>();

    for (var group : groups) {
      StudentGroupDto studentGroupDto = new StudentGroupDto();
      studentGroupDto.setId(group.getId());
      studentGroupDto.setNumber(group.getNumber());
      studentGroupDto.setName(group.getName());

      for (var student : group.getStudents()) {
        StudentDto studentDto = new StudentDto();
        studentDto.setFirstName(student.getName());
        studentDto.setSurname(student.getSurname());
        studentDto.setAge(student.getAge());
        studentDto.setPhoneNumber(student.getStudentInfo().getPhoneNumber());
        studentDto.setAddress(student.getStudentInfo().getAddress());

        for (var teacher : student.getTeachers()) {
          TeacherDto teacherDto = new TeacherDto();
          teacherDto.setId(teacher.getId());
          teacherDto.setName(teacher.getName());
          teacherDto.setAge(teacher.getAge());
          teacherDto.setSurname(teacher.getSurname());

          studentDto.getTeachers().add(teacherDto);
        }

        studentGroupDto.getStudents().add(studentDto);
      }

      groupDtos.add(studentGroupDto);
    }

    return ResponseEntity.ok(groupDtos);
  }

}



