package com.springboot.tutorial.controller;

import com.springboot.tutorial.dto.StudentDto;
import com.springboot.tutorial.service.CircuitBreakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/circuit-breaker")
@RequiredArgsConstructor
public class CircuitBreakerController {

  private final CircuitBreakerService circuitBreakerService;

  @GetMapping("/students/{id}")
  public ResponseEntity<StudentDto> getStudentById(@PathVariable Long id) {
    return ResponseEntity.ok(circuitBreakerService.getStudentById(id));
  }

}
