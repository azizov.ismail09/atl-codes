package com.springboot.tutorial.checker;

import com.springboot.tutorial.exception.InsufficientBalanceException;
import org.springframework.stereotype.Component;

@Component
public class BalanceChecker {

  public void check(double amount) {
    if (amount > 500) {
      throw new InsufficientBalanceException();
    }
  }

}
