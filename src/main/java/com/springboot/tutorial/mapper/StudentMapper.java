package com.springboot.tutorial.mapper;

import com.springboot.tutorial.dto.StudentDto;
import java.util.List;
import org.mapstruct.Mapper;
import com.springboot.tutorial.entity.Student;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "springboot")
public abstract class StudentMapper {

  public static final StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

  @Mapping(target = "firstName", source = "name")
  @Mapping(target = "address", source = "studentInfo.address")
  @Mapping(target = "phoneNumber", source = "studentInfo.phoneNumber")
  @Mapping(target = "adult", source = "age", qualifiedByName = "checkIsAdult")
  public abstract StudentDto entityToDto(Student student);

  @Named("checkIsAdult")
  protected Boolean checkIsAdult(Integer age) {
    return age > 20;
  }

}
