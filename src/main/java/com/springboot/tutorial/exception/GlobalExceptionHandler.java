package com.springboot.tutorial.exception;

import com.springboot.tutorial.dto.ErrorMessage;
import java.time.LocalDateTime;
import javax.validation.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<ErrorMessage> handleException(NotFoundException ex) {
    log.info(ex.getMessage());
    ErrorMessage errorMessage = new ErrorMessage();
    errorMessage.setNow(LocalDateTime.now());
    errorMessage.setStatus(ex.getStatus());
    errorMessage.setErrorCode(ex.getStatus().value());
    errorMessage.setErrorMessage(ex.getMessage());
    return ResponseEntity.status(ex.getStatus()).body(errorMessage);
  }

  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public ResponseEntity<ErrorMessage> handleMultipartException(MaxUploadSizeExceededException ex) {
    log.info(ex.getMessage());
    ErrorMessage errorMessage = new ErrorMessage();
    errorMessage.setNow(LocalDateTime.now());
    errorMessage.setStatus(HttpStatus.BAD_REQUEST);
    errorMessage.setErrorCode(HttpStatus.BAD_REQUEST.value());
    errorMessage.setErrorMessage(ex.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> handleValidationException(MethodArgumentNotValidException ex) {
    log.info(ex.getMessage());
    ErrorMessage errorMessage = new ErrorMessage();
    errorMessage.setNow(LocalDateTime.now());
    errorMessage.setStatus(HttpStatus.BAD_REQUEST);
    errorMessage.setErrorCode(HttpStatus.BAD_REQUEST.value());
    errorMessage.setErrorMessage(ex.getLocalizedMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
  }

  @ExceptionHandler(UsernameNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleException(UsernameNotFoundException ex) {
    log.info(ex.getMessage());
    ErrorMessage errorMessage = new ErrorMessage();
    errorMessage.setNow(LocalDateTime.now());
    errorMessage.setErrorMessage(ex.getMessage());
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorMessage);
  }

  @ExceptionHandler(InvalidArgumentException.class)
  public ResponseEntity<ErrorMessage> handleException(InvalidArgumentException ex) {
    log.info(ex.getMessage());
    ErrorMessage errorMessage = new ErrorMessage();
    errorMessage.setNow(LocalDateTime.now());
    errorMessage.setStatus(ex.getStatus());
    errorMessage.setErrorCode(ex.getStatus().value());
    errorMessage.setErrorMessage(ex.getMessage());
    return ResponseEntity.status(ex.getStatus()).body(errorMessage);
  }

}
