package com.springboot.tutorial.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class InvalidArgumentException extends RuntimeException {

  private final HttpStatus status = HttpStatus.BAD_REQUEST;

  public InvalidArgumentException(String message) {
    super(message);
  }

}
