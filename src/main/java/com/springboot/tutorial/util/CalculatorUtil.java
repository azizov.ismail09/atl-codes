package com.springboot.tutorial.util;

import org.springframework.stereotype.Component;

@Component
public class CalculatorUtil {

  public Integer calc(int a, int b) {
    if (a > b) {
      return a - b;
    } else {
      return b - a;
    }
  }

}
