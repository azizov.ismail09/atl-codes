FROM openjdk
WORKDIR /home
COPY target/tutorial-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "/home/tutorial-0.0.1-SNAPSHOT.jar"]